<?php

defined( "ABSPATH" ) || exit();

define( "VIP_DIR", plugin_dir_path( dirname( __FILE__ ) ) );
define( "VIP_URL", plugin_dir_url( dirname( __FILE__ ) ) );
define( "VIP_INC", VIP_DIR . 'inc/' );
define( "VIP_TPL", VIP_DIR . 'tpl/' );

/*
 * upload path
 */
$upload = wp_upload_dir();
define( 'VIP_UPLOAD_DIR', $upload['basedir'] . DIRECTORY_SEPARATOR . 'vip' . DIRECTORY_SEPARATOR );
define( 'VIP_UPLOAD_URL', $upload['baseurl'] . DIRECTORY_SEPARATOR . 'vip/' );
define( "VIP_BASE_UPLOAD_DIR", $upload['basedir'] . DIRECTORY_SEPARATOR );

define( 'VIP_METABOX_KEY_PLAN', 'vip_metabox' );