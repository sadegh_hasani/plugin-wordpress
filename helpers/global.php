<?php

access_file();

function access_file() {
	defined( "ABSPATH" ) || exit();
}

function vip_get_template( $view, $data = [] ) {

	$view      = str_replace( ".", '/', $view );
	$file_path = VIP_TPL . $view . '.php';
	if ( file_exists( $file_path ) ) {
		extract( $data );

		include $file_path;
	} else {
		echo "file {$file_path} not exist";
		exit();
	}
}


add_action( 'init', function () {
	ob_start();
} );

if ( ! session_id() ) {
	session_start();
}