<?php
access_file();
?>

<div class="message-wrapper">
	<?php if ( isset( $messages ) ): ?>
		<?php foreach ( $messages as $fm ): ?>
            <div class="flash-<?= $fm->type ?>">
                <p><?= $fm->msg ?></p>
            </div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>
