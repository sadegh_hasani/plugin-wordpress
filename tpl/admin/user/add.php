<?php
access_file();
?>

<div class="wrap">
    <h1>
        افزودن کاربر
        <a href="<?= add_query_arg( [ 'action' => null, 'user_id' => null ] ) ?>" class="page-title-action">لیست
            کاربران</a>
    </h1>

    <form action="" method="post">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for=""> کاربر</label>
                </th>
                <td>
					<?php if ( isset( $users ) ): ?>
                        <select name="user_id">
                            <option value="0">-- کاربر را انتخاب کنید --</option>
							<?php foreach ( $users as $user ): ?>
                                <option value="<?= $user->ID ?>"> <?= $user->display_name ?></option>
							<?php endforeach; ?>
                        </select>
					<?php endif; ?>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">
                    <label for=""> طرح </label>
                </th>
                <td>
					<?php if ( isset( $plans ) ): ?>
                        <select name="plan_id">
                            <option value="0">-- طرح را انتخاب کنید --</option>
							<?php foreach ( $plans as $plan ): ?>
                                <option value="<?= $plan->plan_id ?>"> <?= $plan->plan_title ?></option>
							<?php endforeach; ?>
                        </select>
					<?php endif; ?>
                </td>
            </tr>
        </table>
		<?php submit_button( 'افزودن کاربر' ); ?>
    </form>
</div>

