<?php
access_file();
?>

<div class="wrap">
    <h1>
        تغییر موجودی کیف پول کاربر
        <a href="<?= add_query_arg( [ 'action' => null, 'user_id' => null ] ) ?>" class="page-title-action">لیست
            کاربران</a>
    </h1>

    <form action="" method="post">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    نام کاربر
                </th>
                <td>
                    <p>
						<?= isset( $user ) ? $user->display_name : '' ?>
                    </p>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">
                    موجودی کیف پول (تومان)
                </th>
                <td>
					<?= number_format( vip_content::get_user_wallet( $user->ID ) ) ?>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">
                    نوع تغییر
                </th>
                <td>
                    <select name="type" id="">
                        <option value="0">-- مشخص کنید --</option>
                        <option value="1">-- افزایش موجودی--</option>
                        <option value="2">-- کاهش موجودی--</option>
                    </select>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">
                    مقدار (تومان)
                </th>
                <td>
                    <input type="number" name="amount">
                </td>
            </tr>
        </table>
		<?php submit_button(); ?>
    </form>

</div>
