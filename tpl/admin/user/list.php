<?php
access_file();

?>
<div class="wrap">
    <h1>
        لیست کاربران
        <a href="<?= add_query_arg( [ 'action' => 'add' ] ) ?>" class="page-title-action">افزودن کاربر</a>
    </h1>

    <table class="widefat">
        <thead>
        <tr>
            <th>شناسه</th>
            <th>نام کاربر</th>
            <th>طرح فعال</th>
            <th>شناسه طرح</th>
            <th>تاریخ انقضا،</th>
            <th>موجودی کیف پول</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>شناسه</th>
            <th>نام کاربر</th>
            <th>طرح فعال</th>
            <th>شناسه طرح</th>
            <th>تاریخ انقضا،</th>
            <th>موجودی کیف پول</th>
            <th>عملیات</th>
        </tr>
        </tfoot>
		<?php if ( isset( $users ) && count( $users ) > 0 ): ?>
			<?php foreach ( $users as $user ): ?>
                <tr>
                    <td><?= $user->vip_user_id ?></td>
                    <td><?= $user->display_name ?></td>
                    <td><?= $user->plan_title ?></td>
                    <td><?= $user->plan_id ?></td>
                    <td><?= $user->expire_date ?></td>
                    <td><?= number_format( vip_content::get_user_wallet( $user->ID ) ) ?> تومان</td>
                    <td>
                        <a href="<?= add_query_arg( [ 'action' => 'remove', 'user_id' => $user->vip_user_id ] ) ?>">
                            <i class="dashicons dashicons-trash"></i>
                        </a>
                        <a href="<?= add_query_arg( [ 'action' => 'wallet', 'user_id' => $user->ID ] ) ?>">
                            <span class="dashicons dashicons-calculator"></span>
                        </a>
                    </td>
                </tr>
			<?php endforeach; ?>
		<?php endif; ?>
    </table>
</div>
