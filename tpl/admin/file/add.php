<?php
access_file();
?>


<div class="wrap">
    <h1>
        افزودن فایل
        <a href="<?= add_query_arg( [ 'action' => null ] ) ?>" class="page-title-action">لیست فایل</a>
    </h1>

    <form action="" method="post" enctype="multipart/form-data">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    عنوان فایل
                </th>
                <td>
                    <input type="text" name="file_title">
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">
                    انتخاب فایل
                </th>
                <td>
                    <input type="file" name="file">
                </td>
            </tr>
        </table>
		<?php submit_button( 'افزودن فایل' ); ?>
    </form>
</div>