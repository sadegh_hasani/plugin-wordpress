<?php
access_file();
?>

<div class="wrap">
    <h1>
        لیست فایل ها
        <a href="<?= add_query_arg( [ 'action' => 'add' ] ) ?>" class="page-title-action">افزودن فایل</a>
    </h1>

    <table class="widefat">
        <thead>
        <tr>
            <td>شناسه فایل</td>
            <td>عنوان فایل</td>
            <td>سایز</td>
            <td>هــش</td>
            <td>تعداد دانلود</td>
            <td>عملیات</td>
        </tr>
        </thead>

        <tfoot>
        <tr>
            <td>شناسه فایل</td>
            <td>عنوان فایل</td>
            <td>سایز</td>
            <td>هــش</td>
            <td>تعداد دانلود</td>
            <td>عملیات</td>
        </tr>
        </tfoot>
        <tbody>
		<?php if ( isset( $files ) ): ?>
			<?php foreach ( $files as $file ): ?>
                <tr>
                    <td><?= $file->file_id ?></td>
                    <td><?= $file->file_title ?></td>
                    <td><?= $file->file_size ?></td>
                    <td><?= $file->file_hash ?></td>
                    <td><?= $file->file_download_count ?></td>
                    <td>
                        <a href="<?= add_query_arg( [ 'action' => 'delete', 'file_id' => $file->file_id ] ) ?>">
                            <i class="dashicons dashicons-trash"></i>
                        </a>
                    </td>
                </tr>
			<?php endforeach; ?>
		<?php endif; ?>
        </tbody>
    </table>
</div>
