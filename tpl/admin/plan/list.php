<?php access_file(); ?>


<div class="wrap">
    <h1>
        لیست محصولات
        <a href="<?= add_query_arg( [ 'action' => 'add' ] ) ?>" class="page-title-action">افزودن محصول</a>
    </h1>

    <table class="widefat">
        <thead>
        <tr>
            <th>شناسه محصول</th>
            <th>عنوان محصول</th>
            <th>تعداد روزهای فعال</th>
            <th>قیمت( تومان )</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <tbody>

		<?php if ( isset( $plans ) && count( $plans ) > 0 ): ?>
			<?php foreach ( $plans as $plan ): ?>
                <tr>
                    <td><?= $plan->plan_id ?></td>
                    <td><?= $plan->plan_title ?></td>
                    <td><?= $plan->plan_credit ?></td>
                    <td><?= $plan->plan_price ?></td>
                    <td>
                        <a href="<?= add_query_arg( [ 'action' => 'delete' ,'plan_id'=>$plan->plan_id] ) ?>">
                            <span class="dashicons dashicons-trash"></span>
                        </a>
                        <a href="<?= add_query_arg( [ 'action' => 'edit', 'plan_id' => $plan->plan_id ] ) ?>">
                            <span class="dashicons dashicons-edit"></span>
                        </a>
                    </td>
                </tr>
			<?php endforeach; ?>
		<?php else: ?>
            <td>رکوردی یافت نشد</td>
		<?php endif; ?>

        </tbody>
    </table>
</div>