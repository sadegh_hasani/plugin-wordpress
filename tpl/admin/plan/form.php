<?php access_file(); ?>

<div class="wrap">
    <h1>
        افزودن محصول
        <a href="<?= add_query_arg( [ 'action' => null, 'plan_id' => null ] ) ?>" class="page-title-action">لیست
            محصولات</a>
    </h1>

    <form action="" method="post">
        <table class="form-table">
            <tr valign="top">
                <th>عنوان محصول</th>
                <td scope="row">
                    <input type="text" name="plan_title" value="<?= isset( $plan ) ? $plan->plan_title : '' ?>">
                </td>
            </tr>
            <tr valign="top">
                <th>روزهای فعال</th>
                <td scope="row">
                    <input type="text" name="plan_credit" value="<?= isset( $plan ) ? $plan->plan_credit : '' ?>">
                </td>
            </tr>
            <tr valign="top">
                <th>قیمت محصول (تومان)</th>
                <td scope="row">
                    <input type="text" name="plan_price" value="<?= isset( $plan ) ? $plan->plan_price : '' ?>">
                </td>
            </tr>
        </table>
        <input type="hidden" name="pid" value="<?= isset( $plan ) ? $plan->plan_id : 0 ?>">
		<?php submit_button( 'افزودن محصول' ); ?>
    </form>
</div>
