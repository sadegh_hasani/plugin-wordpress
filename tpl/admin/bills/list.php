<?php

access_file(); ?>

<div class="wrap">
    <h1>
        لیست صورت حساب ها
    </h1>

    <table class="widefat">
        <thead>
        <tr>
            <th>شناسه صورت حساب</th>
            <th>شناسه کاربری</th>
            <th>نام کاربری</th>
            <th>نوع</th>
            <th>مبلغ (تومان)</th>
            <th>تاریخ</th>
            <th>توضیحات</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>شناسه صورت حساب</th>
            <th>شناسه کاربری</th>
            <th>نام کاربری</th>
            <th>نوع</th>
            <th>مبلغ (تومان)</th>
            <th>تاریخ</th>
            <th>توضیحات</th>
        </tr>
        </tfoot>
        <tbody>
		<?php if ( isset( $bills ) ): ?>

			<?php foreach ( $bills as $bill ): ?>
                <tr>
                    <td><?= $bill->bill_id ?></td>
                    <td><?= $bill->ID ?></td>
                    <td><?= $bill->display_name ?></td>
                    <td><?= vip_content::get_bill_type( $bill->type ) ?></td>
                    <td><?= $bill->amount ?></td>
                    <td><?= $bill->date ?></td>
                    <td><?= $bill->description ?></td>
                </tr>
			<?php endforeach; ?>
		<?php endif; ?>
        </tbody>
    </table>
</div>

