<?php


access_file();
?>

<div class="wrap">
    <table class="form-table">
        <th>انتخاب طرح</th>
        <td>
            <select name="plan_id" id="">
				<?php if ( isset( $plans ) ): ?>
                    <option value="0">محتوا برای همه</option>
					<?php foreach ( $plans as $plan ): ?>
                        <option value="<?= $plan->plan_id ?>" <?php selected( $plan->plan_id, $plan_id ) ?>>
							<?php echo $plan->plan_title ?>
                        </option>
					<?php endforeach; ?>
				<?php endif; ?>
            </select>
        </td>
    </table>
</div>
