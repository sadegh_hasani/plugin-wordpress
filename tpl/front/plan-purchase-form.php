<?php
access_file();

?>

<?php echo vip_flash_message::show_message() ?>

<form method="post" action="">

    <div class="plan-form-warp">
        <div class="vip-form-row">
            <label for=""> انتخاب محصول</label>
            <select name="plan_selected" id="">
				<?php if ( isset( $plans ) ): ?>
                    <option value="0">-- محصول خود را انتخاب کنید --</option>
					<?php foreach ( $plans as $plan ): ?>
                        <option value="<?= $plan->plan_id ?>"><?= $plan->plan_title ?></option>
					<?php endforeach; ?>
				<?php else: ?>
                    <option value="0">-- در حال حاظر محصولی وجود ندارد --</option>
				<?php endif; ?>
            </select>
        </div>
    </div>
    <div class="vip-form-row">
        <input type="submit" class="button button-primary" value="خرید محصول" name="submit">
    </div>
</form>