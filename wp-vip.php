<?php
/*
 * plugin name: کاربران ویژه
 * author: sadegh haasni
 * author url: https://www.google.com
 * description: پلاگین نمونه کار
 */

defined( "ABSPATH" ) || exit();


require "bootstrap/constants.php";
require "helpers/global.php";

final class user_vip {

	private static $instance = null;

	public static function get_instance() {
		if ( self::$instance == null ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function __construct() {
		$this->init();
		$this->includes();
		is_admin() ? $this->register_menus() : null;
		! is_admin() ? vip_plan_purchase::get_instance() : null;
		is_admin() ? $this->register_vip_metabox() : null;
		!is_admin() ? $this->register_vip_filter_content() : null;
	}

	private function includes() {

		include VIP_INC . 'vip-db/contract/class-vip-db-contract.php';
		include VIP_INC . 'vip-db/contract/class-vip-db.php';

		if ( is_admin() ) {
			include VIP_INC . 'vip-db/class-vip-bills.php';
			include VIP_INC . 'vip-db/class-vip-files.php';
			include VIP_INC . 'pages/dashboard.php';
			include VIP_INC . 'pages/plans.php';
			include VIP_INC . 'pages/users.php';
			include VIP_INC . 'pages/bills.php';
			include VIP_INC . 'pages/files.php';
			include VIP_INC . 'class-admin-menus.php';
			include VIP_INC . 'class-vip-upload.php';
			include VIP_INC . 'metaboxes/class-vip-metaboxes.php';

		}

		if ( ! is_admin() ) {
			include VIP_INC . 'shortcodes/class-vip-shortcodes.php';
			include VIP_INC . 'class-vip-filter-content.php';
		}

		include VIP_INC . 'vip-db/class-vip-plans.php';
		include VIP_INC . 'vip-db/class-vip-users.php';

		include VIP_INC . 'class-vip-content.php';
		include VIP_INC . 'utilities/class-vip-flash-message.php';

	}

	public function init() {
		register_activation_hook( __FILE__, [ $this, 'user_vip_activation' ] );
		register_deactivation_hook( __FILE__, [ $this, 'user_vip_deactivation' ] );
	}

	public function user_vip_activation() {

	}

	public function user_vip_deactivation() {

	}

	public function register_menus() {
		vip_menus::get_instance();
	}

	function register_vip_metabox() {
		vip_metaboxes::get_instance();
	}

	function register_vip_filter_content() {
		vip_filter_content::get_instance();
	}
}

user_vip::get_instance();