<?php

access_file();

class files {

	private $file_model;

	public function __construct() {
		$this->file_model = new vip_files();

		add_action( 'vip_file_add', [ $this, 'add' ] );

	}

	public function index() {

		$page = $_GET['action'] ?? null;

		switch ( $page ) {
			case 'add':
				do_action( 'vip_file_add' );
				break;
			default:
				$data = [
					'files' => $this->file_model->get_tables()
				];
				vip_get_template( 'admin.file.list', $data );
				break;
		}
	}

	public function add() {
		if ( isset( $_POST['submit'] ) ) {

			$file        = new vip_upload( 'file' );
			$file_upload = $file->file_uploaded();

			if ( $file_upload ) {
				$file_name  = $file->save();
				$file_hash  = $file->file_hash( $file_name );
				$file_title = $_POST['file_title'] ? sanitize_text_field( $_POST['file_title'] ) : '';


				$this->file_model->insert(
					[
						'file_title'          => $file_title,
						'file_name'           => $file_name,
						'file_hash'           => $file_hash,
						'file_size'           => $file->size(),
						'file_download_count' => 0
					],
					[ '%s', '%s', '%s', '%d', '%d', ]
				);
			}


		}

		vip_get_template( 'admin.file.add' );
	}
}