<?php

access_file();

class bills {

	private $bill_model;

	public function __construct() {

		$this->bill_model = new vip_bills();
	}

	public function index() {

		$action = $_GET['action'] ?? null;

		switch ( $action ) {
			default:
				$data = [
					'bills' => $this->bill_model->get_bills()
				];
				vip_get_template( 'admin.bills.list', $data );
				break;
		}
	}


}