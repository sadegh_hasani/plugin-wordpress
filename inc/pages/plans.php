<?php


access_file();


class plans {

	private $plan_model;

	public function __construct() {
		$this->plan_model = new vip_plans();
		add_action( 'vip_add_plan', [ $this, 'add' ] );
		add_action( 'vip_edit_plan', [ $this, 'edit' ] );
		add_action( 'vip_delete_plan', [ $this, 'delete' ] );
	}

	public function index() {

		$data   = [
			'plans' => $this->plan_model->get_tables()
		];
		$action = $_GET['action'] ?? null;

		switch ( $action ) {
			case 'add':
				do_action( 'vip_add_plan' );
				break;
			case 'edit':
				$plan_id = $_GET['plan_id'] ?? 0;
				do_action( 'vip_edit_plan', $plan_id );
				break;
			case "delete":
				$plan_id = $_GET['plan_id'] ?? 0;
				do_action( 'vip_delete_plan', $plan_id );
			default:
				vip_get_template( 'admin.plan.list', $data );
		}
	}

	public function add() {
		global $wpdb;
		if ( isset( $_POST['submit'] ) ) {
			$title  = $_POST['plan_title'] ? sanitize_text_field( $_POST['plan_title'] ) : null;
			$credit = $_POST['plan_credit'] ? intval( $_POST['plan_credit'] ) : null;
			$price  = $_POST['plan_price'] ? intval( $_POST['plan_price'] ) : null;
			$this->plan_model->insert(
				[
					'plan_title'  => $title,
					'plan_credit' => $credit,
					'plan_price'  => $price,
				],
				[ '%s', '%d', '%d' ]
			);

			echo $wpdb->last_error;
		}
		vip_get_template( 'admin.plan.form' );
	}

	public function delete( $plan_id ) {

		$this->plan_model->delete( [ 'plan_id' => $plan_id ], [ '%d' ] );

		wp_redirect( admin_url( 'admin.php?page=vip_plans' ) );
		exit();
	}

	public function edit( $plan_id ) {

		if ( isset( $_POST['submit'] ) ) {
			$plan_title  = $_POST['plan_title'];
			$plan_credit = $_POST['plan_credit'];
			$plan_price  = $_POST['plan_price'];
			$plan_id     = $_POST['pid'];

			$this->plan_model->update(
				[
					'plan_title'  => $plan_title,
					'plan_credit' => $plan_credit,
					'plan_price'  => $plan_price,
				],
				[ 'plan_id' => $plan_id ],
				[ '%s', '%d', '%d' ]
			);
		}

		$data = [
			'plan' => $this->plan_model->select( [ 'plan_id' => $plan_id ] )
		];

		vip_get_template( 'admin.plan.form', $data );
	}

}