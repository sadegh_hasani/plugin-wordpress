<?php

access_file();

class users {

	private $user_model;

	private $plan_model;

	private $bill_model;

	const VIP_USER_PAGE = 'admin.php?page=vip_users';
	const VIP_USER_PAGE_ADD = 'admin.php?page=vip_users&action=add';

	public function __construct() {

		$this->user_model = new vip_users();

		$this->plan_model = new vip_plans();

		$this->bill_model = new vip_bills();

		add_action( 'vip_add_user', [ $this, 'add' ] );

		add_action( 'vip_remove_user', [ $this, 'remove' ] );

		add_action( 'vip_wallet_user', [ $this, 'wallet' ] );
	}

	public function index() {
		$action  = $_GET['action'] ?? null;
		$user_id = $_GET['user_id'] ?? null;

		switch ( $action ) {
			case 'add':
				do_action( 'vip_add_user' );
				break;
			case 'remove':

				do_action( 'vip_remove_user', $user_id );
				break;
			case 'wallet':

				do_action( 'vip_wallet_user', $user_id );
				break;
			default:
				$data = [ 'users' => $this->user_model->users() ];
				vip_get_template( 'admin.user.list', $data );
				break;
		}
	}

	public function add() {

		if ( isset( $_POST['submit'] ) ) {
			$user_id    = intval( $_POST['user_id'] );
			$plan_id    = intval( $_POST['plan_id'] );
			$plan       = $this->plan_model->select( [ 'plan_id' => $plan_id ] );
			$user_exist = $this->user_model->select( [ 'user_id' => $user_id ] );

			if ( ( $user_id <= 0 ) && ( $plan_id <= 0 ) ) {

				wp_redirect( 'admin.php?page=vip_users&action=add' );
				exit();

			}

			if ( $user_exist ) {

				wp_redirect( 'admin.php?page=vip_users&action=add' );
				exit();

			}

			if ( $plan ) {

				$expire_date = date( "Y-m-d H:i:s", strtotime( "+ {$plan->plan_credit} days" ) );
				$this->user_model->insert(
					[
						'user_id'     => $user_id,
						'plan_id'     => $plan_id,
						'expire_date' => $expire_date
					],
					[ '%d', '%d', '%s' ]
				);
			}

		}

		$data = [
			'users' => get_users( array( 'fields' => array( 'ID', 'display_name' ) ) ),
			'plans' => $this->plan_model->get_tables()
		];

		vip_get_template( 'admin.user.add', $data );
	}

	public function remove( $user_id ) {

		if ( ! $user_id ) {
			return;
		}
		$remove = $this->user_model->delete( [ 'vip_user_id' => $user_id ] );
		if ( $remove ) {
			wp_redirect( admin_url( self::VIP_USER_PAGE ) );
			exit();
		}

	}

	public function wallet( $user_id ) {
		if ( ! $user_id ) {
			return;
		}
		if ( isset( $_POST['submit'] ) ) {

			$type   = intval( $_POST['type'] );
			$amount = intval( $_POST['amount'] );

			if ( $type <= 0 && $amount == 0 ) {
				wp_redirect( admin_url( self::VIP_USER_PAGE_ADD ) );
				exit();
			}

			vip_content::update_user_wallet( $user_id, $amount, $type );
			$data = date( "Y-m-d H:i:s" );

			$this->bill_model->insert(

				[
					'user_id'     => $user_id,
					'type'        => $type,
					'amount'      => $amount,
					'date'        => $data,
					'description' => 'تغییر موجودی کیف پول توسط مدیر سایت'
				],
				[ '%d', '%d', '%d', '%s', '%s' ]
			);

		}
		$data = [ 'user' => wp_get_current_user() ];

		vip_get_template( 'admin.user.wallet', $data );
	}


}