<?php


access_file();

class vip_plan_purchase {

	private static $instance = null;

	private $plan_model;

	private $user_model;

	public static function get_instance() {
		if ( self::$instance == null ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function __construct() {

		$this->plan_model = new vip_plans();

		$this->user_model = new vip_users();

		add_shortcode( 'vip_buy_plan', [ $this, 'vip_purchase_form' ] );
	}

	public function vip_purchase_form() {
		global $post;
		if ( isset( $_POST['submit'] ) ) {
			$plan_id    = $_POST['plan_selected'];
			$user       = wp_get_current_user();
			$user_exist = $this->is_user_vip( $user->ID );


			if ( ! $this->plan_exist( $plan_id ) && $plan_id == 0 ) {
				vip_flash_message::add( 'محصول مد نظر در دسترس نمیباشد', vip_flash_message::ERROR );
				wp_redirect( home_url( $post->post_name ) );
				exit();
			}
			if ( $user_exist ) {
				vip_flash_message::add( 'در حال حاظر شما طرح کاربر ویژه برای  فعال است لطفا بعد از اتمام تاریخ طرح اقدام نمایید', vip_flash_message::ERROR );
				wp_redirect( home_url( $post->post_name ) );
				exit();
			}

		}

		$data = [
			'plans' => $this->plan_model->get_tables()
		];
		vip_get_template( 'front.plan-purchase-form', $data );
	}

	private function add_user2vip_plan( $user_id, $plan_id ) {
		if ( ! $user_id && ! $plan_id ) {
			return false;
		}

		$plan = $this->plan_model->select( [ 'plan_id' => $plan_id ] );

		if ( $plan ) {

			$this->user_model->insert( [
				'user_id'     => $user_id,
				'plan_id'     => $plan_id,
				'expire_date' => date( "Y-m-d H:i:m", strtotime( "+ {$plan->plan_credit} days" ) )
			], [
				'%d',
				'%d',
				'%s'
			] );
		}
	}

	private function is_user_vip( $user_id ) {
		if ( ! $user_id ) {
			return false;
		}

		$user = $this->user_model->select( [ 'user_id' => $user_id ] );

		return intval( $user->user_id ) ? true : false;
	}

	private function plan_exist( $plan_id ) {
		$plan = $this->plan_model->select( [ 'plan_id' => $plan_id ] );

		return $plan->plan_id ? true : false;
	}



}