<?php

access_file();

interface vip_db_contract {

	public function get_tables();

	public function insert( $data = array(), $format = array() );

	public function update( $data = [], $where = [], $format = [] );

	public function delete( $where = [], $format = [] );

	public function select( $where = [] );

}