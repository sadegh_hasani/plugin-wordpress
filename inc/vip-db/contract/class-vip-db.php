<?php

access_file();

class vip_db implements vip_db_contract {

	protected static $wpdb;
	protected static $table;
	protected static $prefix;


	public function __construct() {
		global $wpdb, $table_prefix;
		self::$wpdb   = $wpdb;
		self::$prefix = $table_prefix;

	}

	public function get_tables() {

		$tables = self::$wpdb->get_results( "SELECT * FROM " . static::$prefix . static::$table );

		return $tables;
	}

	public function insert( $data = [], $format = [] ) {
		$add = self::$wpdb->insert( static::$prefix . static::$table, $data, $format );

		return $add;
	}

	public function update( $data = [], $where = [], $format = [] ) {
		$update = self::$wpdb->update( static::$prefix . static::$table, $data, $where, $format );

		return $update;
	}

	public function delete( $where = [], $format = [] ) {
		$delete = self::$wpdb->delete( static::$prefix . static::$table, $where, $format );

		return $delete;
	}

	public function select( $where = [] ) {
		$where = $this->arr_to_str( $where );;
		$field = self::$wpdb->get_row( "SELECT * FROM " . static::$prefix . static::$table . " WHERE {$where}" );


		return $field;

	}

	private function arr_to_str( $array ) {
		$result = '';
		foreach ( $array as $item => $value ) {
			$result .= $item . '=' . $value;
		}

		return $result;

	}


}