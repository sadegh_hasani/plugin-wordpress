<?php

access_file();

class vip_users extends vip_db {
	protected static $table = 'vip_users';

	public function users() {
		$table_prefix = self::$prefix;
		$users        = self::$wpdb->get_results( "
		SELECT vu.*, u.ID,display_name,vp.plan_id,plan_title
		FROM {$table_prefix}" . self::$table . " vu 
		JOIN {$table_prefix}users u
		ON vu.user_id = u.ID
		JOIN {$table_prefix}vip_plans vp 
		ON vu.plan_id = vp.plan_id
		" );

		return $users;
	}

}