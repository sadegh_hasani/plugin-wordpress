<?php

access_file();

class vip_bills extends vip_db {
	protected static $table = 'vip_bills';

	public function get_bills() {
		$bill_table = self::$prefix . self::$table;
		$user_table = self::$wpdb->users;
		$bills      = self::$wpdb->get_results( "
		SELECT vb.*,u.ID,display_name 
		FROM {$bill_table} vb
		JOIN {$user_table} u
		on vb.user_id =u.ID
		" );

		return $bills;
	}
}