<?php
access_file();

class vip_metaboxes {

	const VIP_KEY_POST_META = 'vip_metabox';

	private $plan_model;

	private static $instance = null;


	public static function get_instance() {
		if ( self::$instance == null ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function __construct() {
		$this->plan_model = new vip_plans();

		add_action( 'add_meta_boxes', [ $this, 'register_vip_metabox' ] );

		add_action( 'save_post', [ $this, 'save_data_metabox' ] );
	}

	public function register_vip_metabox() {
		add_meta_box(
			'vip_metabox',
			'انتخاب محتوا برای کاربران ویژه',
			[ $this, 'vip_metabox_content' ],
			'post', 'normal'
		);
	}

	public function vip_metabox_content( $post ) {

		$data = [
			'plans'   => $this->plan_model->get_tables(),
			'plan_id' => get_post_meta( $post->ID, self::VIP_KEY_POST_META, true )
		];


		vip_get_template( 'admin.metabox.metabox', $data );
	}

	public function save_data_metabox( $post_id ) {

		if ( ! isset( $post_id ) ) {
			return false;
		}

		$plan_id = isset( $_POST['plan_id'] ) ? intval( $_POST['plan_id'] ) : 0;

		if ( $plan_id ) {
			update_post_meta( $post_id, self::VIP_KEY_POST_META, $plan_id );
		}
	}

}