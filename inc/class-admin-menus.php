<?php

access_file();

class vip_menus {

	private static $instance = null;

	public static function get_instance() {
		if ( self::$instance == null ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function __construct() {
		add_action( 'admin_menu', [ $this, 'register_user_vip_menus' ] );
	}

	function register_user_vip_menus() {

		add_menu_page(
			'کاربران ویژه',
			'کاربران ویژه',
			'manage_options',
			'wp_vip',
			null,
			'dashicons-universal-access-alt',
			6
		);

		add_submenu_page(
			'wp_vip',
			'کاربران ویژه',
			'داشبورد',
			'manage_options',
			'wp_vip',
			[ new dashboard(), 'index' ]
		);

		add_submenu_page(
			'wp_vip',
			'محصولات',
			'محصولات',
			'manage_options',
			'vip_plans',
			[ new plans(), 'index' ]
		);

		add_submenu_page(
			'wp_vip',
			'کاربران',
			'کاربران',
			'manage_options',
			'vip_users',
			[ new users(), 'index' ]

		);

		add_submenu_page(
			'wp_vip',
			'فایل',
			'فایل',
			'manage_options',
			'vip_files',
			[ new files(), 'index' ]
		);

		add_submenu_page(
			'wp_vip',
			'صورت حساب',
			'صورت حساب',
			'manage_options',
			'vip_bills',
			[ new bills(), 'index' ]
		);
	}


}

