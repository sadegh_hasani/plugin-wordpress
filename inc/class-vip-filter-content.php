<?php

access_file();

class vip_filter_content {

	private static $instance = null;

	private $user_model;

	public static function get_instance() {
		if ( self::$instance == null ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function __construct() {

		$this->user_model = new vip_users();

		add_filter( 'the_content', [ $this, 'filter_content' ] );
	}

	public function filter_content( $content ) {
		global $post;
		$user     = wp_get_current_user();
		$plan_id  = get_post_meta( $post->ID, VIP_METABOX_KEY_PLAN, true );
		$user_vip = $this->allowed_view_post( $user->ID );

		if ( ! $plan_id ) {
			return $content;
		}

		if ( $user_vip ) {
			return $content;
		}

		return "محتوای برا یکاربران ویژه";

	}

	private function allowed_view_post( $user_id ) {
		if ( ! $user_id ) {
			return false;
		}

		$vip_user = $this->user_model->select( [ 'user_id' => $user_id ] );

		return $vip_user ? $vip_user->user_id : 0;
	}
}