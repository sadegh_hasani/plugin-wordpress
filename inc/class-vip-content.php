<?php

access_file();

class vip_content {

	const VIP_WALLET = 'vip_user_wallet';

	public static function get_user_wallet( $user_id ) {
		if ( ! $user_id ) {
			return;
		}

		$wallet = get_user_meta( $user_id, self::VIP_WALLET, true );

		return intval( $wallet );
	}

	public static function update_user_wallet( $user_id, $amount, $type ) {
		if ( ! $user_id ) {
			return false;
		}
		if ( $amount == 0 && $type == 0 ) {
			return false;
		}
		$user_wallet = self::get_user_wallet( $user_id );
		if ( $type == 1 ) {
			update_user_meta( $user_id, self::VIP_WALLET, $user_wallet + intval( $amount ) );
		}
		if ( $type == 2 ) {
			update_user_meta( $user_id, self::VIP_WALLET, $user_wallet - intval( $amount ) );
		}
	}

	public static function get_bill_type( $type ) {
		$str = '';

		if ( $type == 1 ) {
			$str = 'پرداخت';
		}

		if ( $type == 2 ) {
			$str = 'برداشت';
		}

		return $str;
	}
}