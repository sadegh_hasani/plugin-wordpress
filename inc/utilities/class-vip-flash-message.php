<?php

access_file();

class vip_flash_message {

	const SUCCESS = 'success';

	const ERROR = 'error';

	const WARNING = 'warning';

	const INFO = 'info';

	const FLASH_MESSAGE = 'flash_message';

	public static function add( $msg, $type = self::SUCCESS ) {
		if ( ! $_SESSION[ self::FLASH_MESSAGE ] ) {
			$_SESSION[ self::FLASH_MESSAGE ] = [];
		}
		$_SESSION[ self::FLASH_MESSAGE ] [] = (object) [ 'msg' => $msg, 'type' => $type ];
	}

	public static function get_message() {
		return $_SESSION[ self::FLASH_MESSAGE ] ?? [];
	}

	public static function clear() {
		if ( isset( $_SESSION[ self::FLASH_MESSAGE ] ) ) {
			$_SESSION[ self::FLASH_MESSAGE ] = [];
		}
	}

	public static function show_message() {
		$messages = self::get_message();
		$data     = [
			'messages' => $messages
		];
		vip_get_template( 'notice.flash-message', $data );
		self::clear();
	}

}