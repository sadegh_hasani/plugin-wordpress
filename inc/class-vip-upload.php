<?php

access_file();

class vip_upload {
	private $file;
	private $upload_path;

	public function __construct( $file_name, $sub_folder = null ) {

		$this->file = $_FILES[ $file_name ];

		if ( is_null( $sub_folder ) ) {
			$vip_folder = VIP_UPLOAD_DIR;
			if ( ! file_exists( $vip_folder ) ) {
				@mkdir( $vip_folder );
			}
		}

		if ( ! is_null( $sub_folder ) ) {
			echo 'no nul';
			$vip_folder = VIP_BASE_UPLOAD_DIR . $sub_folder . DIRECTORY_SEPARATOR;
			if ( ! file_exists( $vip_folder ) ) {
				@mkdir( $vip_folder );
			}
		}

		$this->upload_path = $vip_folder . $this->basename() . $this->extension();
	}

	public function mimeType() {
		return $this->file['type'];
	}

	public function size() {
		return $this->file['size'];
	}

	public function name() {
		return substr( $this->file['name'], 0, 128 );
	}

	public function file_uploaded() {
		return ( $this->file['error'] == 4 ) ? false : true;
	}

	public function extension() {

		return pathinfo( $this->file['name'], PATHINFO_EXTENSION );
	}

	public function basename() {
		return basename( $this->name(), $this->extension() );
	}

	private function upload( $path ) {
		return move_uploaded_file( $this->file['tmp_name'], $path );
	}

	public function file_name_uploaded() {
		return $this->basename() . $this->extension();
	}

	public function save() {
		if ( $this->file_uploaded() ) {
			$upload = $this->upload( $this->upload_path );
			if ( $upload ) {
				return $this->file_name_uploaded();
			}

			return false;
		}

		return false;
	}

	public function file_hash( $file ) {
		if ( $this->file_uploaded() ) {
			return md5_file( VIP_UPLOAD_DIR . $file );
		}

		return false;
	}
}

